
package ArrayList;

import java.util.ArrayList;
import java.util.Iterator;

public class List 
{
    public static void main(String[] args) 
    {
        ArrayList list = new ArrayList();
        
        list.add("Rijwan");  // index = 0
        list.add(5);         // index = 1
        list.add(58);        // index = 2
        list.add(14);        // index = 3
        list.add(72);        // index = 4
        
        list.set(1, 56);     // value replaced in index = 1
        
        list.remove(3);     // value of index = 3 deleted
        
        int a = list.size(); // a = 4
        System.out.println("ArrayList Size = " + a);
        
        list.clear();        // Delete all values
        
        Iterator it = list.iterator();
        while(it.hasNext())
        {
            System.out.println(it.next());
        }
        
        
    }
}











